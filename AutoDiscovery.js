const Influx = require("influx");
const fs = require("fs");
const reader = require('readline-sync');
var configJsonData = JSON.parse(fs.readFileSync('./config.json', 'utf-8'));
var database = "Swagat";
const influx = new Influx.InfluxDB({
    database: database,
    host: configJsonData.hostid,
    port: configJsonData.port,
    username: (configJsonData.username.length == 0) ? "root" : configJsonData.username,
    password: (configJsonData.password.length == 0) ? "root" : configJsonData.password
});
let jsonData = JSON.parse(fs.readFileSync('./metrics.json', 'utf-8'));
jsonData.host = influx._options.hosts[0].host;
jsonData.port = influx._options.hosts[0].port;
jsonData.username = influx._options.username;
jsonData.password = influx._options.password;
jsonData.firstProcess = 0;
jsonData.append = false;
getDb();
function getDb() {
    influx.getDatabaseNames()
        .then(names => {
            console.log('\x1b[33m%s\x1b[0m',"The database names are :-")
            for (var i = 0; i < names.length; i++) {
                console.log(`${i + 1}: ${names[i]}`)
            }
            var dbid = reader.question(`Select the id of the database you want to select\n`)
            for (var i = 0; i < names.length; i++) {
                if (dbid == i + 1) {
                    db_name = names[i];
                    jsonData.database = db_name;
                    influx._options.database = db_name;
                    break
                }
            }
            getMeasurements();
        })
        .catch(err => {
            console.log("\"error during getDb() execution\"=" + err);
        });
}
function getMeasurements() {
        influx.getMeasurements()
            .then(names => {
                console.log('The measurement names are: ' + names)
                jsonData.measurement = [];
                for (i = 0; i < names.length; i++) {
                    jsonData.measurement.push({ "measurement_name": names[i] });
                }
                getTagKeys()
            })
            .catch(err => console.log("\"error during getMeasurements() execution\"=" + err));
}
function getTagKeys() {
    influx.query(`show tag keys`).
        then(names => {
            console.log(names)
            console.log("-----------------");
            jsonData.tags = [];
            for (i = 0; i < names.length; i++) {
                if (names[i].hasOwnProperty("tagKey")) {
                    jsonData.tags.push({ "tagKey": names[i].tagKey });
                }
            }
            getFieldKeys();
        }).catch(error => {
            console.log("\"error during getTagKeys() execution\"=" + error)
        })
}
function getFieldKeys() {
    influx.query(`show field keys`).
        then(names => {
            console.log(names);
            jsonData.fields = [];
            for (i = 0; i < names.length; i++) {
                if (names[i].hasOwnProperty("fieldKey")) {
                    jsonData.fields.push({ "fieldKey": names[i].fieldKey, "fieldType": names[i].fieldType });
                }
            }
            console.log('\x1b[33m%s\x1b[0m',"Your data is ready in metrics.json.\nPlease make other required changes if necessary");
            console.log('\x1b[41m%s\x1b[0m',"**************************************");
            console.log('\x1b[33m%s\x1b[0m',"Current metrics datas are :-")
            console.log('\x1b[44m%s\x1b[0m',"--------------------------------------")
            console.log(jsonData);
            fs.writeFileSync("./metrics.json", JSON.stringify(jsonData), (err) => {
                console.log(err);
            })
        }).catch(error => {
            console.log("\"error during getFieldKeys() execution\"=" + error)
        })
}

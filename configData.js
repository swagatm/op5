const axios = require('axios')
const qs = require('querystring')
var fs = require("fs")
//var configDSJson = {}
var metricsData = JSON.parse(fs.readFileSync("./metrics.json", { encoding: 'utf8', flag: 'r' }))
var accId = metricsData.accountId;
//console.log(accId)
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const requestBody = {
    client_id: metricsData.configDsData[0].client_id,
    username: metricsData.configDsData[0].username,
    password: metricsData.configDsData[0].password,
    grant_type: metricsData.configDsData[0].grant_type
}
const config = {
    headers: {
        'Content-Type': metricsData.configDsData[0].Content_Type_Token
    }
}
var accObj = {};
var agentlist = []
var agenttypename = metricsData.configDsData[0].agenttypename
var data = async function () {
    var configDSJson;
    return new Promise(function (resolve, reject) {
        axios.post(`https://${metricsData.configDsData[0].configDsIP}:${metricsData.configDsData[0].configDsPort}/auth/realms/master/protocol/openid-connect/token`, qs.stringify(requestBody), config)
            .then(async (result) => {
                //console.log(result.data.refresh_token);
                var k = await callConfig(result.data.refresh_token, configDSJson);
                resolve(k)
            })
            .catch((err) => {
                console.log(err)
            })
    })
}
exports.data = data()
async function callConfig(token, configDSJson) {
    return new Promise((resolve, reject) => {
        axios.get(`https://${metricsData.configDsData[0].configDsIP}:${metricsData.configDsData[0].configDsPort}/appsone-controlcenter/v1.0/api/configData`, {
            headers: {
                "Authorization": `${token}`,
                'Content-Type': metricsData.configDsData[0].Content_Type_DsData
            }
        })
            .then(async (response) => {
                //console.log(response.data.result);
                var result = response.data.result;
                for (var i = 0; i < result.length; i++) {
                    if (result[i].accountId == accId) {
                        accObj = result[i];
                        agentlist = accObj.agentList;
                        //console.log(accObj)
                    }
                }
                var k = await getKPIdata(configDSJson, agentlist);
                resolve(k);
            })
            .catch(function (error) {
                console.log(error);
            })
    })
}
async function getKPIdata(configDSJson, agentlist) {
    //console.log(result)
    return new Promise((resolve, reject) => {
        var map = {};
        for (var i = 0; i < agentlist.length; i++) {
            var compInstanceids = [];
            if (agentlist[i].agentTypeName == agenttypename) {
                var id = agentlist[i].agentId;
                for (var k = 0; k < agentlist[i].compInstanceIds.length; k++) {
                    compInstanceids.push(agentlist[i].compInstanceIds[k])
                }
                var singleInstance = {};
                for (var k = 0; k < accObj.compInstanceDetail.length; k++) {
                    if (compInstanceids.includes(+(accObj.compInstanceDetail[k].instanceId), 0)) {
                        var kpis = [];
                        for (var l = 0; l < accObj.compInstanceDetail[k].kpis.length; l++) {
                            var kpi = {};
                            if (accObj.compInstanceDetail[k].kpis[l].isGroup == false) {
                                kpi["kpiType"] = accObj.compInstanceDetail[k].kpis[l].kpiType;
                                kpi["isKpiGroup"] = accObj.compInstanceDetail[k].kpis[l].isGroup;
                                kpi["kpiUid"] = accObj.compInstanceDetail[k].kpis[l].id;
                                kpi["kpiName"] = accObj.compInstanceDetail[k].kpis[l].name;
                                kpi["collectionInterval"] = accObj.compInstanceDetail[k].kpis[l].collectionInterval;
                                kpis.push(kpi);
                            }
                        }
                        singleInstance[accObj.compInstanceDetail[k].identifier] = kpis;
                    }
                }
                map[id] = singleInstance;
            }
        }
        //console.log(map)
        configDSJson = JSON.stringify(map);
        //fs.writeFileSync('./ConfigMapped.json',JSON.stringify(map));
        exports.configDSJson = configDSJson;
        resolve(configDSJson)
    })

}


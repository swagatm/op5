// If you want to monitor more distinct tag types, Then make changes in selectDistinctTag,reformArray methods
//require('events').EventEmitter.defaultMaxListeners = 100000;
const Influx = require('influx');
var influx;
const sleep = require("sleep")
const fs = require("fs");
const reader = require('readline-sync');
var moment = require('moment');
var chunkSize;
var newArr = [];
var configdata = require("./configData.js");
let jsonData = JSON.parse(fs.readFileSync('./metrics.json', 'utf-8'));
var tempData;
var process = require("process");
if (!fs.existsSync("./op5connector.pid")) {
    fs.writeFileSync("./op5connector.pid", process.pid)
}
else if (fs.existsSync("./op5connector.pid")) {
    console.log('\x1b[33m%s\x1b[0m', `The previous op5connector process with  PID "${fs.readFileSync("./op5connector.pid").toString()}" did not stop correctly.
Please wait for it to end, or terminate it,or delete the "op5connector.pid" file before attempting to run op5connector again...`)
    process.exit();
}

if (jsonData.append == true) {
    fs.appendFileSync("./TempData.json", "]");
    var stream = JSON.parse(fs.readFileSync("./TempData.json"))
    stream[stream.length - 1].firstProcess = 1;
    stream = stream[stream.length - 1]
    fs.writeFileSync("./TempData.json", JSON.stringify(stream));
    tempData = JSON.parse(fs.readFileSync("./TempData.json"));
}
else {
    tempData = JSON.parse(fs.readFileSync("./TempData.json"))
}

jsonData.firstProcess = tempData.firstProcess;
jsonData.fStartTime = tempData.fStartTime;
fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
var configDSData;
var measurement_name;
if (jsonData.firstProcess != 0) {
    restartProcess();
}
else {
    selectMeasurement();
}
function restartProcess() {
    var fq = reader.question(`Do you want to continue from last process?\n
Here are the previous configurations
--------------------------------------------------------------------------
accountId = ${jsonData.accountId}
last Selected Database = ${jsonData.database}
last Selected Measurement = ${jsonData.lastMeasurement}
last Selected Tag = ${jsonData.lastTag}
last Selected distinct ${jsonData.lastTag} type = ${jsonData.distinctTag}
chunkSize = ${jsonData.ChunkSize}
select data = ${jsonData.select}
mode = ${jsonData.loadmode ? "loadmode" : "runmode"}
selected service to Input Data = ${jsonData.servicename}
Time frame = ${jsonData.iStartTime} to ${jsonData.EndTime}
Time at which Process Stopped = ${jsonData.fStartTime}
Final Query = SELECT ${jsonData.select} FROM "${jsonData.lastMeasurement}" WHERE ("time" >= '${jsonData.fStartTime}' AND "time" < '${jsonData.EndTime}') LIMIT ${jsonData.ChunkSize}
--------------------------------------------------------------------------
`)
    if (fq == "y" || fq == "yes" || fq == "Y" || fq == "Yes" || fq == "YES") {
        jsonData.firstProcess = 1;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
        makeSchemaRestart();
    }
    else if (fq == "n" || fq == "no" || fq == "N" || fq == "No" || fq == "NO") {
        jsonData.firstProcess = 0;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
        selectMeasurement();

    }
}
function makeSchemaRestart() {
    influx = new Influx.InfluxDB({
        host: jsonData.host,
        database: jsonData.database,
        port: jsonData.port,
        username: jsonData.username,
        password: jsonData.password,
        schema: [{
            measurement: jsonData.lastMeasurement,
            fields: {},
            tags: []
        }]
    })
    var element = "";
    for (let i = 0; i < jsonData.tags.length - 1; i++) {
        element += jsonData.tags[i].tagKey + ", ";
    }
    element += jsonData.tags[jsonData.tags.length - 1].tagKey;
    influx._options.schema[0].tags.push(element);
    var fieldObj = {};
    for (let i = 0; i < jsonData.fields.length; i++) {
        var key = jsonData.fields[i].fieldKey;
        var datatype = jsonData.fields[i].fieldType;
        fieldObj[`${key}`] = `Influx.FieldType.${datatype.toUpperCase()}`
    }
    influx._options.schema[0].fields = fieldObj;
    getConfigData()
}

function selectMeasurement() {
    for (var i = 0; i < jsonData.measurement.length; i++) {
        console.log(`${i + 1}:-${jsonData.measurement[i].measurement_name}`)
    }
    var mea = reader.question("Select the id of the measurement you want to get data from\n");
    for (var i = 0; i < jsonData.measurement.length; i++) {
        if (mea == i + 1) {
            measurement_name = jsonData.measurement[i].measurement_name;
            jsonData.lastMeasurement = measurement_name;
            break
        }

    }
    if (measurement_name == undefined) {
        console.log(`Wrong id selected.\n No measurement present with that id.\n Exiting process`)
        process.exit()
    }
    chunkSize = reader.question(`Fetch data at chunk size of ${jsonData.ChunkSize}?\n
If "NO" then type new chunk size here\nIf "YES" type "0"\n`);
    if (chunkSize == 0) {
        chunkSize = jsonData.ChunkSize;
    }
    else {
        jsonData.ChunkSize = chunkSize;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData), (err) => {
            console.log(err);
        })
    }
    makeSchema();
}
function makeSchema() {
    influx = new Influx.InfluxDB({
        host: jsonData.host,
        database: jsonData.database,
        port: jsonData.port,
        username: jsonData.username,
        password: jsonData.password,
        schema: [{
            measurement: measurement_name,
            fields: {},
            tags: []
        }]
    })
    var element = "";
    for (let i = 0; i < jsonData.tags.length - 1; i++) {
        element += jsonData.tags[i].tagKey + ", ";
    }
    element += jsonData.tags[jsonData.tags.length - 1].tagKey;
    influx._options.schema[0].tags.push(element);
    var fieldObj = {};
    for (let i = 0; i < jsonData.fields.length; i++) {
        var key = jsonData.fields[i].fieldKey;
        var datatype = jsonData.fields[i].fieldType;
        fieldObj[`${key}`] = `Influx.FieldType.${datatype.toUpperCase()}`
    }
    influx._options.schema[0].fields = fieldObj;
    for (var i = 0; i < jsonData.tags.length; i++) {
        console.log(`${i + 1}:-${jsonData.tags[i].tagKey}`)
    }
    var tagId = reader.question(`Select the id of the tag according to which you want to filter your data\n`)
    var tag;
    for (var i = 0; i < jsonData.tags.length; i++) {
        if (tagId == i + 1) {
            tag = jsonData.tags[i].tagKey
            jsonData.lastTag = tag
            fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
            break;
        }
    }
    influx.query(`SHOW TAG VALUES FROM ${measurement_name} WITH KEY = ${tag}`)
        .then(async (res) => {
            await selectDistinctTag(res, tag);
            getConfigData()
        })
        .catch(err => {
            console.log(err)
        })
}
async function selectDistinctTag(res, tag) {
    jsonData.distinctTag = [];
    fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
    return new Promise((resolve, reject) => {
        do {
            for (var i = 0; i < res.length; i++) {
                if (res[i].hasOwnProperty('key')) {
                    console.log(`${i + 1}:-${res[i].value}`)
                }
            }
            var hostS;
            var hostId = +(reader.question(`Select the id of the distinct ${tag} type:-\n`));
            for (var i = 0; i < res.length; i++) {
                if (hostId == i + 1) {
                    hostS = res[i].value;
                    jsonData.distinctTag.push(hostS);
                    hostS = "";
                    fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
                    break;
                }
            }
            var more = reader.question(`Do you want to select more distinct ${tag}?\nIf "Yes" press "y"\nIf "No" press "n"\n`);
        } while (more != "n")

        for (var i = 0; i < jsonData.distinctTag.length; i++) {
            if (jsonData.distinctTag[i] != "Shopify" && jsonData.distinctTag[i] != "monitor") {

                console.log(`We are not monitoring data for ${jsonData.distinctTag[i]} type.Hence removing ${jsonData.distinctTag[i]} from memory`)
                jsonData.distinctTag.splice(i, 1);
                fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));

            }
        }
        resolve("Success");
    })
}
async function getConfigData() {
    var k = await configdata.data;
    configDSData = JSON.parse(configdata.configDSJson);
    //console.log(configDSData)
    if (jsonData.firstProcess == 1) {
        formqueryRestart(influx);
    }
    else {
        pushIntoService(configDSData);
    }
}
function pushIntoService(configDSData) {
    jsonData.servicename = [];
    fs.writeFileSync("./metrics.json", JSON.stringify(jsonData))
    for (var k = 0; k < Object.keys(configDSData[Object.keys(configDSData)[0]]).length; k++) {
        console.log(`${k + 1}:-${Object.keys(configDSData[Object.keys(configDSData)[0]])[k]}`)
    }
    // console.log(`0:-Exit Process`)

    var number = [];
    var q = {};
    var w = [];
    for (var ll = 0; ll < jsonData.distinctTag.length; ll++) {
        var kk = +(reader.question(`Push ${jsonData.distinctTag[ll]} data in :- `));
        number.push(kk);
    }
    var servicename;
    // if (number == 0) process.exit();
    for (var tt = 0; tt < number.length; tt++) {
        for (var k = 0; k < Object.keys(configDSData[Object.keys(configDSData)[0]]).length; k++) {
            if (number[tt] == k + 1) {
                servicename = Object.keys(configDSData[Object.keys(configDSData)[0]])[k];
                jsonData.servicename.push(servicename)
                fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
                q["DistinctTypes"] = jsonData.distinctTag[tt];
                q[`servicename`] = servicename;
                q[`ArrayName`] = `${jsonData.distinctTag[tt]}Arr`;
                w.push(q);
                q = {};
                jsonData["FinalData"] = w;
                break;
            }
        }
    }
    fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
    setDaysOfData(influx);
}
function setDaysOfData(influx) {
    if (jsonData.loadmode == true) {
        var days = +jsonData.DaysOfdata;
        var hours = +jsonData.HoursOfData;
        var minutes = +jsonData.MinutesOfData;
        var totalDays = days + (hours / 24) + (minutes / (24 * 60));
        // var FSL = jsonData.FixStartTimeForLoadMode;
        var FslConfirm = reader.question(`Fetching data before ${jsonData.FixStartTimeForLoadMode}.\nDo you want to change it?Press  'y' for Yes  , 'n' for "No"\n`)
        if (FslConfirm == "y") {
            var nd = reader.question(`Type new date here in "yyyy-mm-dd HH:mm:ss" format\n`)
            jsonData.FixStartTimeForLoadMode = nd;
            fs.writeFileSync("./metrics.json", JSON.stringify(jsonData))
        }
        var ndays = reader.question(`Fetch data for last ${totalDays} days from ${jsonData.FixStartTimeForLoadMode}?
If "NO" then type number of days here\nIf "YES" type "0"\n`)
        if (ndays != 0) {
            jsonData.DaysOfdata = ndays;
            jsonData.HoursOfData = 0;
            jsonData.MinutesOfData = 0;
            fs.writeFileSync("./metrics.json", JSON.stringify(jsonData), (err) => {
                console.log(err);
            })
        }
        var chunkHeal = reader.question(`Sending ${jsonData.chunkSizeForHeal} data per minute to Heal.Do you want to change it?
 If "NO" then type "n".
 If "YES" then type the number here\n`)
        if (chunkHeal != "n") {
            jsonData.chunkSizeForHeal = chunkHeal;
            fs.writeFileSync("./metrics.json", JSON.stringify(jsonData), (err) => {
                console.log(err);
            })
        }
        setTimeout(() => {
            formquery(influx);
        }, 1 * 700)
    }
}
function formquery(influx) {
    if (jsonData.loadmode == true) {
        var select = jsonData.select;
        var days = +jsonData.DaysOfdata;
        var hours = +jsonData.HoursOfData;
        var minutes = +jsonData.MinutesOfData;
        var totalMinutes = days * 24 * 60 + hours * 60 + minutes;
        var endTime;
        var now;
        if (jsonData.FixStartTimeForLoadMode.length == 0) {
            now = moment().format(`YYYY-MM-DD HH:mm:ss`);
            endTime = moment(now).subtract(totalMinutes, 'minutes').format(`YYYY-MM-DD HH:mm:ss`);
            endTime = `${endTime.substr(0, 17)}00`
        }
        else {
            now = jsonData.FixStartTimeForLoadMode;
            endTime = moment(now).subtract(totalMinutes, 'minutes').format(`YYYY-MM-DD HH:mm:ss`);
            endTime = `${endTime.substr(0, 17)}00`
        }
        console.log(`SELECT ${select} FROM "${influx._options.schema[0].measurement}" WHERE ("time" >= '${endTime}' AND "time" < '${now}' AND "${jsonData.lastTag}" = '${jsonData.distinctTag[0]}'${checkDistinctTagSize()}) LIMIT ${chunkSize}`);
        jsonData.iStartTime = endTime;
        jsonData.EndTime = now;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
        checkQueryChunk(influx, endTime, now, select);
    }
}
function checkDistinctTagSize() {
    if (jsonData.distinctTag.length > 1) {
        var k = "";
        for (var i = 1; i < jsonData.distinctTag.length; i++) {
            k += ` OR "${jsonData.lastTag}" = '${jsonData.distinctTag[i]}'`
        }
        return k
    }
    return ""
}
function formqueryRestart(influx) {
    if (jsonData.loadmode == true) {
        var select = jsonData.select;
        var endTime;
        var now;
        chunkSize = +jsonData.ChunkSize;
        endTime = jsonData.fStartTime;
        now = jsonData.EndTime;
        console.log(`SELECT ${select} FROM "${influx._options.schema[0].measurement}" WHERE ("time" >= '${endTime}' AND "time" < '${now}' AND "${jsonData.lastTag}" = '${jsonData.distinctTag[0]}'${checkDistinctTagSize()}) LIMIT ${chunkSize}`);
        jsonData.iStartTime = endTime;
        jsonData.EndTime = now;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
        checkQueryChunk(influx, endTime, now, select);
    }
}
function checkQueryChunk(influx, endTime, now, select) {
    setTimeout(() => {
        influx.query(`SELECT ${select} FROM "${influx._options.schema[0].measurement}" WHERE ("time" >= '${endTime}' AND "time" <= '${now}' AND "${jsonData.lastTag}" = '${jsonData.distinctTag[0]}'${checkDistinctTagSize()}) LIMIT ${chunkSize}`)
            .then(receivedData => {
                if (receivedData.length < chunkSize) {
                    lastScanQueryData(receivedData);
                    //process.exit();
                }
                checkQueryDate(receivedData, influx, endTime, now, select);
            }).catch(error => { console.log("\"error during checkQueryChunk() execution\"=" + error) })
    }, 1 * 1000)
}
function checkQueryDate(receivedData, influx, endTime, now, select) {
    var i = 1;
    while (i <= 1000) {
        var upTime = moment(moment(receivedData[receivedData.length - i].time._nanoISO).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        var downTime = moment(moment(receivedData[receivedData.length - i - 1].time._nanoISO).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        if (upTime.substr(0, 16) == downTime.substr(0, 16)) {
            i++;
            continue;
        }
        else {
            executeQuery(influx, endTime, downTime, select, upTime, now);
            break
        }
    }
}
function executeQuery(influx, endTime, downTime, select, upTime, now) {
    setTimeout(() => {
        influx.query(`SELECT ${select} FROM "${influx._options.schema[0].measurement}" WHERE ("time" >= '${endTime}' AND "time" <= '${downTime}' AND "${jsonData.lastTag}" = '${jsonData.distinctTag[0]}'${checkDistinctTagSize()})`)
            .then(queryData => {
                scanQueryData(queryData, influx, upTime, now, select);
            })
            .catch(err => {
                console.log("\"error during executeQuery() execution\"=" + err);
            })
    }, 1 * 1500)
}
function scanQueryData(queryData, influx, upTime, now, select) {
    var myJson = {};
    var myArr = [];
    for (j = 0; j < queryData.length; j++) {
        if (jsonData.distinctTag.includes(queryData[j].host)) {
            var str = queryData[j].time._nanoISO;
            myJson["time"] = str.substr(0, 16);
            myJson["service"] = queryData[j].service;
            myJson["value"] = queryData[j].value;
            myArr.push(myJson);
            myJson = {};
        }
    }
    if (myArr.length != 0) {
        scanQueryDataInArray(myArr, influx, upTime, now, select)
    } else {
        checkQueryChunk(influx, upTime, now, select);
    }
}
function scanQueryDataInArray(myArr, influx, upTime, now, select) {
    var counter = 1;
    var sum = 0;
    var avg = 1;
    for (k = 1; k < myArr.length; k++) {
        if (myArr[k - 1].time == myArr[k].time && myArr[k - 1].service == myArr[k].service) {
            counter++;
            if (counter == 2) {
                sum = +(myArr[k - 1].value) + (+myArr[k].value);
                continue
            }
            else {
                sum += +(myArr[k].value);
            }
            if (k == myArr.length - 1) {
                avg = sum / counter;
                myArr.splice(k - counter + 2, counter - 1);
                myArr[k - counter + 1].value = avg;
                k = k - counter + 2;
                counter = 1;
                sum = 0;
                avg = 1;
            }
        }
        else if (counter != 1) {
            avg = sum / counter;
            myArr.splice(k - counter + 1, counter - 1);
            myArr[k - counter].value = avg;
            k = k - counter + 1;
            counter = 1;
            sum = 0;
            avg = 1;
        }
    }
    // console.log(myArr);
    ArrayToString(myArr, influx, upTime, now, select);
}
function ArrayToString(myArr, influx, upTime, now, select) {
    var newStr = {};
    for (m = 0; m < myArr.length; m++) {
        // console.log(myArr[m].time)
        newStr["time"] = moment(moment(myArr[m].time).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        newStr[`${myArr[m].service}`] = myArr[m].value;
        newArr.push(newStr);
        newStr = {};
    }
    //Remove duplicate objects
    for (var k = 1; k < newArr.length; k++) {
        if (newArr[k - 1].time == newArr[k].time) {
            var aa = Object.keys(newArr[k])[1];
            newArr[k - 1][`${Object.keys(newArr[k])[1]}`] = newArr[k][aa];
            newArr.splice(k, 1);
            k--;
        }
    }
    // Add for missing timestamp
    for (var k = 1; k < newArr.length; k++) {
        myJson = {};
        var next = moment(moment(moment(newArr[k - 1].time).add(1, "minutes")).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        if (next != newArr[k].time) {
            myJson["time"] = next;
            newArr.splice(k, 0, myJson);
            myJson = {};
        }
    }
    checkQueryChunk(influx, upTime, now, select);
}
function lastScanQueryData(queryData) {
    var myJson = {};
    var myArr = [];
    for (j = 0; j < queryData.length; j++) {
        if (jsonData.distinctTag.includes(queryData[j].host)) {
            var str = queryData[j].time._nanoISO;
            myJson["time"] = str.substr(0, 16);
            myJson["service"] = queryData[j].service;
            myJson["value"] = queryData[j].value;
            // myJson["unit"] = queryData[j].unit;
            myArr.push(myJson);
            myJson = {};
        }
    }
    if (myArr.length == 0) {
        process.exit();
    }
    lastScanQueryDataInArray(myArr);
}
function lastScanQueryDataInArray(myArr) {
    var counter = 1;
    var sum = 0;
    var avg = 1;
    for (k = 1; k < myArr.length; k++) {
        if (myArr[k - 1].time == myArr[k].time && myArr[k - 1].service == myArr[k].service) {
            counter++;
            if (counter == 2) {
                sum = +(myArr[k - 1].value) + (+myArr[k].value);
                continue
            }
            else {
                sum += +(myArr[k].value);
            }
            if (k == myArr.length - 1) {
                avg = sum / counter;
                myArr.splice(k - counter + 2, counter - 1);
                myArr[k - counter + 1].value = avg;
                k = k - counter + 2;
                counter = 1;
                sum = 0;
                avg = 1;
            }
        }
        else if (counter != 1) {
            avg = sum / counter;
            myArr.splice(k - counter + 1, counter - 1);
            myArr[k - counter].value = avg;
            k = k - counter + 1;
            counter = 1;
            sum = 0;
            avg = 1;
        }
    }
    lastArrayToString(myArr);
}
function lastArrayToString(myArr) {
    var newStr = {};
    for (m = 0; m < myArr.length; m++) {
        newStr["time"] = moment(moment(myArr[m].time).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        newStr[`${myArr[m].service}`] = myArr[m].value;
        newArr.push(newStr);
        newStr = {};
    }
    for (var k = 1; k < newArr.length; k++) {
        if (newArr[k - 1].time == newArr[k].time) {
            var aa = Object.keys(newArr[k])[1];
            newArr[k - 1][`${Object.keys(newArr[k])[1]}`] = newArr[k][aa];
            newArr.splice(k, 1);
            k--;
        }
    }
    for (var k = 1; k < newArr.length; k++) {
        myJson = {};
        var next = moment(moment(moment(newArr[k - 1].time).add(1, "minutes")).utcOffset("+00:00")).format(`YYYY-MM-DD HH:mm:ss`);
        if (next != newArr[k].time) {
            myJson["time"] = next;
            newArr.splice(k, 0, myJson);
            myJson = {};
        }
    }
    reformArray(newArr, configDSData)
    //fs.writeFileSync("May1to4Component.json", JSON.stringify(newArr));
    //process.exit();

}
function reformArray(newArr, configDSData) {
    //console.log(JSON.stringify(newArr))
    var sampleArr = [];
    var sampleObj = {};
    var fo = {};
    var far = [];
    var myTempObj = {};
    var myTempArr = [];
    for (var a = 0; a < jsonData.distinctTag.length; a++) {
        if (Object.keys(jsonData.kpiNames[0]).includes(jsonData.distinctTag[a])) {
            var dt = jsonData.distinctTag[a]
            //console.log(dt)
            for (var j = 0; j < Object.keys(jsonData.kpiNames[0][dt][jsonData.kpiNames[0][dt].length - 1]).length; j++) {
                var k = Object.keys(jsonData.kpiNames[0][dt][jsonData.kpiNames[0][dt].length - 1])[j];
                if (jsonData.kpiNames[0][dt][jsonData.kpiNames[0][dt].length - 1][k] != "") {
                    myTempObj[k] = jsonData.kpiNames[0][dt][jsonData.kpiNames[0][dt].length - 1][k]
                }
            }
        }
        myTempArr.push(myTempObj)
        myTempObj = {};
        //console.log(myTempArr)
        for (var i = 0; i < newArr.length; i++) {
            for (var k = 0; k < Object.keys(newArr[i]).length; k++) {
                sampleObj["timeInGMT"] = newArr[i].time;
                // console.log(newArr[i][k]);
                if (Object.keys(myTempArr[0]).includes(Object.keys(newArr[i])[k])) {
                    if (newArr[i][Object.keys(newArr[i])[k]] != 0) {
                        sampleObj[myTempArr[0][Object.keys(newArr[i])[k]]] = newArr[i][Object.keys(newArr[i])[k]]
                    }
                    else {
                        sampleObj[myTempArr[0][Object.keys(newArr[i])[k]]] = 0;
                    }
                }

            }
            sampleArr.push(sampleObj)
            sampleObj = {}
        }

        //console.log(sampleArr)
        //incase Any Data i.e Shopify or monitor doesn't exist
        for (var i = 0; i < sampleArr.length; i++) {
            //console.log("aaa1")
            //incase Any Data i.e Shopify or monitor doesn't exist (some kpis only)
            try {
                if (Object.keys(sampleArr[i]).length > 1 && Object.keys(sampleArr[i]).length < Object.keys(myTempArr[0]).length) {
                    console.log("aaa2")
                    for (var j = 0; j < Object.keys(myTempArr[0]).length; j++) {
                        if (!Object.keys(sampleArr[i]).includes(myTempArr[0][Object.keys(myTempArr[0])[j]])) {
                            sampleArr[i][myTempArr[0][Object.keys(myTempArr[0])[j]]] = 0;
                        }
                    }
                }
                //incase Any Data i.e Shopify or monitor doesn't exist at whole
                else if (Object.keys(sampleArr[i]).length == 1) {
                    //console.log("aaa3")
                    //sampleObj["timeInGMT"] = sampleArr[i].time;
                    for (var j = 0; j < Object.keys(myTempArr[0]).length; j++) {
                        //sampleObj[myTempArr[0][Object.keys(myTempArr[0])[j]]] = 0;
                        sampleArr[i][myTempArr[0][Object.keys(myTempArr[0])[j]]] = 0;
                    }
                    //console.log(sampleArr)
                    // sampleArr.splice(i, 0, sampleObj)
                    // sampleObj = {}
                }
            }
            catch (err) {
                console.log(err)
            }

        }



        //console.log(sampleArr)
        fo[`${jsonData.distinctTag[a]}Arr`] = sampleArr;
        sampleArr = [];
        myTempArr = [];
    }
    far.push(fo);
    fo = {};
    formProtoData(far, configDSData)
}
function formProtoData(far, configDSData) {
    var protoArr = [];
    var protoObj = {};
    var kd = {};
    var inst = {};

    // fs.writeFileSync("newArr1to4Component.json", JSON.stringify(far));
    // console.log(jsonData)
    // console.log(far[0][Object.keys(far[0])[0]].length);
    // console.log(jsonData.FinalData.length)
    for (var ii = 0; ii < far[0][Object.keys(far[0])[0]].length; ii++) {
        for (var kkk = 0; kkk < jsonData.FinalData.length; kkk++) {
            var resultArr = far[0][jsonData.FinalData[kkk].ArrayName];
            var service = jsonData.FinalData[kkk].servicename;
            var serviceArr = configDSData[Object.keys(configDSData)[0]][service];
            for (var k = 0; k < serviceArr.length; k++) {
                if ((Object.keys(resultArr[ii])).includes(serviceArr[k].kpiName)) {
                    kd["timeInGMT"] = resultArr[ii].timeInGMT;
                    kd["kpiName"] = serviceArr[k].kpiName;
                    kd["kpiUid"] = serviceArr[k].kpiUid;
                    kd["collectionInterval"] = serviceArr[k].collectionInterval;
                    kd["val"] = resultArr[ii][serviceArr[k].kpiName]
                    inst["instanceId"] = service;
                    inst["kpiData"] = kd;
                    protoObj["agentUid"] = Object.keys(configDSData)[0];
                    protoObj["instances"] = inst;
                    protoArr.push(protoObj);
                    protoObj = {};
                    inst = {};
                    kd = {};
                }
            }
        }
    }
    sendProtoData(protoArr);
}
// function sendProtoData(protoArr) {
//     var b = 0;
//     var counter = 0;
//     setInterval(async () => {
//         if (jsonData.firstProcess == 1) {
//             counter++;
//             if (counter == 1) {
//                 b = await checkInterruptedData(protoArr, jsonData.lastSentDataObject)
//                 console.log(JSON.stringify(protoArr[b]))
//             }
//             else if (counter > 1) {
//                 console.log(JSON.stringify(protoArr[b]))
//             }
//         }
//         else if (jsonData.firstProcess == 0) {
//             console.log(JSON.stringify(protoArr[b]))
//         }
//         // jsonData.fStartTime = protoArr[b].instances.kpiData.timeInGMT;
//         // fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
//         process.on('SIGINT', function (signal) {
//             jsonData.fStartTime = protoArr[b].instances.kpiData.timeInGMT;
//             jsonData.firstProcess = 1;
//             jsonData.lastSentDataObject = protoArr[b]
//             fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
//             process.exit(0);
//         });
//         console.log(b,protoArr.length)
//         if (b < protoArr.length-1){
//             b++;
//         }
//         else if(b == protoArr.length-1){
//             // console.log(true)
//             //fs.writeFileSync("./May1to4Component.json",JSON.stringify(protoArr))
//             jsonData.firstProcess = 0;
//             fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
//             //clearInterval(refreshid)
//             process.exit()
//         }
//     }, 10)();
// }

// async function checkInterruptedData(protoArr, protoobj) {
//     return new Promise((resolve, reject) => {
//         for (var i = 0; i < protoArr.length; i++) {
//             if (protoobj.instances.kpiData.timeInGMT == protoArr[i].instances.kpiData.timeInGMT && protoobj.instances.kpiData.kpiName == protoArr[i].instances.kpiData.kpiName) {
//                 resolve(i);
//                 break
//             }
//         }
//         reject("Kuch to gadbad he Daya")
//     })
// }
function sendProtoData(protoArr) {
    jsonData.append = true;
    fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
    fs.open("./TempData.json", "w", async (err, fd) => {
        var fIrstProcess = 1;
        var n = 1;
        var m;
        for (var i = 0; i < protoArr.length; i++) {
            if (i == 0) {
                console.time();
                m = +moment(moment().valueOf())
            }
            var fstartTime = protoArr[i].instances.kpiData.timeInGMT;
            //jsonData.lastSentDataObject = protoArr[i]
            if (i == 0)
                fs.writeSync(fd, `[{"fStartTime": "${fstartTime}","firstProcess": ${fIrstProcess}}`)
            else if (i == protoArr.length - 1)
                fs.writeSync(fd, `,{"fStartTime": "${fstartTime}","firstProcess": ${fIrstProcess}}]`)
            else
                fs.writeSync(fd, `,{"fStartTime": "${fstartTime}","firstProcess": ${fIrstProcess}}`)
            await printData(JSON.stringify(protoArr[i]))
            console.log(i, n)
            if (i == n * jsonData.chunkSizeForHeal) {
                n++
                var b = +moment(moment().valueOf())
                console.timeEnd();
                sleep.msleep(60000 - (b - m))
                console.time()
                m = +moment(moment().valueOf());
            }
        }
        fs.close(fd, () => {
        });
        var stream = JSON.parse(fs.readFileSync("./TempData.json"))
        stream[stream.length - 1].firstProcess = 0;
        stream = stream[stream.length - 1]
        fs.writeFileSync("./TempData.json", JSON.stringify(stream));
        jsonData.append = false;
        fs.writeFileSync("./metrics.json", JSON.stringify(jsonData));
        fs.unlinkSync("./op5connector.pid")
        process.exit();
    })
}
// const sleep1 = ms => {
//     return new Promise(resolve => setTimeout(resolve, ms))
// }
const printData = result => {
    return new Promise((resolve, reject) => {
        console.log(result)
        resolve(1);
    })
}
// async function final(protoArr){
//     return new Promise((resolve,reject)=>{
//         console.log("1")
//         fs.open(__dirname + "/TempData.json", "w", (err, fd) => {
//             console.log("3")
//             console.log(err);
//             var fIrstProcess = 1;
//             for (var i = 0; i < protoArr.length; i++) {
//                 var fstartTime = protoArr[i].instances.kpiData.timeInGMT;
//                 //jsonData.lastSentDataObject = protoArr[i]
//                 fs.writeSync(fd, `[{"fStartTime": "${fstartTime}","firstProcess": ${fIrstProcess}}]`)
//                 console.log(JSON.stringify(protoArr[i]))
//             }
//             fs.close(fd, () => {
//             });
//         })
//         resolve(process.cwd());
//     }) 
// }